import Foundation

public struct JustResourcesDLib {
    public static var name = "JustResourcesDLib"
    
    public init() {}
    
    public static func printModuleInfo() {
        let module = Bundle.module
        
        print("bundleIdentifier = \(module.bundleIdentifier ?? "nil")")
        print("      bundlePath = \(module.bundlePath)")
        print("  executablePath = \(module.executablePath ?? "nil")")
        print("    resourcePath = \(module.resourcePath ?? "nil")")
    }
}
