    import Foundation
    import XCTest
    @testable import JustResourcesDLib

    final class JustResourcesDLibTests: XCTestCase {
        func testExample() {
            // This is an example of a functional test case.
            // Use XCTAssert and related functions to verify your tests produce the correct
            // results.

            JustResourcesDLib.printModuleInfo()
            
            XCTAssertEqual(JustResourcesDLib.name, "JustResourcesDLib")
        }
    }
