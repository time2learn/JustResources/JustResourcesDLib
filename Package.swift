// swift-tools-version:5.4
// Package: JustResourcesDLib

import PackageDescription

let package = Package(
    name: "JustResourcesDLib",
    platforms: [
        // specify each minimum deployment requirement,
        // otherwise the platform default minimum is used.
        .iOS(.v14),
        .macOS(.v11), // .v10_14 Mojave, .v10_15 Catalina
        .tvOS(.v14),
        .watchOS(.v7),
    ],
    products: [
        // `Products` defines and make executables and libraries visible to other packages.
        .library(
            name: "JustResourcesDLib",
            type: .static, // .dynamic, .static
            targets: ["JustResourcesDLib"]
        ),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. 
        // A target can define a module or a test suite.
        // Targets can depend on other targets in this package, 
        // and on products in packages this package depends on.
        .target(
            name: "JustResourcesDLib",
            dependencies: [],
            resources: [.copy("Resources/"),]
        ),
        .testTarget(
            name: "JustResourcesDLibTests",
            dependencies: ["JustResourcesDLib"]
        ),
    ],
    // -- Optionally, specify the minimum language version --
    swiftLanguageVersions: [SwiftVersion.v5]
    //cLanguageStandard: CLanguageStandard.c11,
    //cxxLanguageStandard: CXXLanguageStandard.cxx14
)
